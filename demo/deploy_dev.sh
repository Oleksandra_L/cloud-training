#!/bin/bash 
docker stop $(docker ps -a -q) 
docker system prune --all --force 
docker run -d -p 80:8080 registry.gitlab.com/oleksandra_l/cloud-training:latest
