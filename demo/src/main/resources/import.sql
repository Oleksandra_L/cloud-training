
insert into product (id, description,name, price) values(1,  'Hardcover', 'Effective Java', 10.5);
insert into product (id, description,name, price) values(2,  'eText', 'Effective Docker', 20.5);
insert into product (id, description,name, price) values(3,  'Online Course','Awesome AWS', 30.5);
insert into product (id, description,name, price) values(4,  'Online Course','Awesome Statistics', 40.5);
insert into product (id, description,name, price) values(5,  'Hardcover','Statistics Handbook', 50.5);

insert into category (id, description,name) values(1,  'Computer Science & IT description', 'Computer Science & IT');
insert into category (id, description,name) values(2,  'Science and Engineering description', 'Science and Engineering');
insert into category (id, description,name) values(3,  'IT Professional description','IT Professional');
insert into category (id, description,name) values(4,  'Math & Statistics description','Math & Statistics');
insert into category (id, description,name) values(5,  'Statistics description','Statistics');

insert into product_category ( product_id,category_id) values(1, 1);
insert into product_category ( product_id,category_id) values(1, 3);
insert into product_category ( product_id,category_id) values(2, 1);
insert into product_category ( product_id,category_id) values(2, 2);
insert into product_category ( product_id,category_id) values(2, 3);
insert into product_category ( product_id,category_id) values(3, 3);
insert into product_category ( product_id,category_id) values(4, 4);
insert into product_category ( product_id,category_id) values(4, 5);
insert into product_category ( product_id,category_id) values(5, 5);

