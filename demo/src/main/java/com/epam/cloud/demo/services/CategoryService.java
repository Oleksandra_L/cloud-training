package com.epam.cloud.demo.services;

import com.epam.cloud.demo.entities.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;


public interface CategoryService
{
	List<Category> getAll();

	List<Category> getAll(Sort sort);

	Page<Category> getAll(Pageable pageable);

	Category save(Category category);

	Category get(Long id);

	void delete(Long id);

	Category update(long id, Category category);
}
