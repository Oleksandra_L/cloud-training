package com.epam.cloud.demo.search;

import com.epam.cloud.demo.entities.Category;
import com.epam.cloud.demo.entities.Product;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


public class ProductSpecificationHolder
{

	public static final Map<String, Function<String, Specification<Product>>> PRODUCT_PROPERTY_SPECIFICATION_MAP = new HashMap<>();

	static
	{
		PRODUCT_PROPERTY_SPECIFICATION_MAP.put("name:", (value) -> containsIn(value,"name"));
		PRODUCT_PROPERTY_SPECIFICATION_MAP.put("description:", (value) -> containsIn(value,"description"));
		PRODUCT_PROPERTY_SPECIFICATION_MAP.put("price>", (value) -> isGreater(value, "price"));
		PRODUCT_PROPERTY_SPECIFICATION_MAP.put("price<", (value) -> isLess(value, "price"));
	}

	public static Specification<Product> containsIn(String namePart, String field)
	{
		return (product, cq, cb) -> cb.like(product.get(field),"%" + namePart+"%");
	}

	public static Specification<Product> isGreater(String value, String field)
	{
		return (product, cq, cb) -> cb.greaterThan(product.get(field), value);
	}

	public static Specification<Product> isLess(String value, String field)
	{
		return (product, cq, cb) -> cb.lessThan(product.get(field), value);
	}

	public static Specification<Product> belongsToCategory(long id)
	{
		return (product, query, cb) -> {
			query.distinct(true);
			Root<Product> cat = product;
			Subquery<Category> categorySubQuery = query.subquery(Category.class);
			Root<Category> category = categorySubQuery.from(Category.class);
			Expression<Collection<Product>> categoryProducts = category.get("products");
			categorySubQuery.select(category);
			categorySubQuery.where(cb.equal(category.get("id"), id), cb.isMember(cat, categoryProducts));
			return cb.exists(categorySubQuery);
		};
	}





}
