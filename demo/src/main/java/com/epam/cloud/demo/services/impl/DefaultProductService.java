package com.epam.cloud.demo.services.impl;

import com.epam.cloud.demo.dao.ProductRepository;
import com.epam.cloud.demo.entities.Category;
import com.epam.cloud.demo.entities.Product;
import com.epam.cloud.demo.exceptions.ProductNotFoundException;
import com.epam.cloud.demo.services.CategoryService;
import com.epam.cloud.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DefaultProductService implements ProductService
{

	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private CategoryService categoryService;

	public Page<Product> getAll(Pageable pageable)
	{
		return productRepository.findAll(pageable);
	}

	@Override
	public List<Product> getAll(Specification<Product> specification)
	{
		return productRepository.findAll(specification);
	}

	public Page<Product> getAll(Pageable pageable, Specification<Product> specification)
	{
		return productRepository.findAll(specification, pageable);
	}

	public Product save(Product product)
	{
		return productRepository.save(product);
	}

	public Product get(Long id)
	{
		return productRepository.findById(id).orElseThrow(ProductNotFoundException::new);
	}

	public void delete(Long id)
	{
		productRepository.deleteById(id);
	}

	@Override
	public Product assignCategory(long productId, long categoryId)
	{
		Product product = get(productId);
		Category category = categoryService.get(categoryId);
		product.addCategory(category);
		return productRepository.save(product);
	}

	@Override
	public Product unassignCategory(long productId, long categoryId)
	{
		Product product = get(productId);
		Category category = categoryService.get(categoryId);
		product.removeCategory(category);
		return productRepository.saveAndFlush(product);
	}

	@Override
	public Product update(long id, Product product)
	{
		Product productToUpdate = get(id);
		productToUpdate.setName(product.getName());
		productToUpdate.setDescription(product.getDescription());
		return productRepository.saveAndFlush(productToUpdate);
	}
}
