package com.epam.cloud.demo.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Formula;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PostLoad;
import javax.persistence.Transient;
import java.util.Set;


@Entity
public class Category
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String description;

	@ManyToMany(mappedBy = "categories")
	@JsonIgnore
	private Set<Product> products;

	@Formula("(SELECT COUNT(pc.product_id) FROM product_category pc WHERE pc.category_id=id)")
	private int totalProducts;

	public Category()
	{
	}

	public Category(String name, String description)
	{
		this.name = name;
		this.description = description;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Set<Product> getProducts()
	{
		return products;
	}

	public void setProducts(Set<Product> products)
	{
		this.products = products;
	}

	public int getTotalProducts()
	{
		return totalProducts;
	}

	public void setTotalProducts(int totalProducts)
	{
		this.totalProducts = totalProducts;
	}

	public void addProduct(Product product)
	{
		this.products.add(product);
		product.getCategories().add(this);
	}

	public void removeProduct(Product product)
	{
		this.products.remove(product);
		product.getCategories().remove(this);
	}

}
