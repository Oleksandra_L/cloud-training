package com.epam.cloud.demo.sort;

import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class SortBuilder
{

	private final List<SortCriterion> sortCriteria;

	public SortBuilder()
	{
		this.sortCriteria = new ArrayList<>();
	}

	public SortBuilder with(String property, String direction)
	{
		sortCriteria.add(new SortCriterion(property, direction));
		return this;
	}

	public Sort build()
	{
		if (sortCriteria.size() == 0)
		{
			return Sort.by(Sort.Direction.ASC, "id");
		}

		List<Sort> sorts = sortCriteria.stream()
				.map(criterion -> Sort.by(criterion.getDirection(), criterion.getProperty()))
				.collect(Collectors.toList());

		Sort foregroundSort = sorts.get(0);

		return IntStream.rangeClosed(1, sorts.size() - 1)
				.boxed()
				.map(sorts::get)
				.reduce(foregroundSort, Sort::and);
	}
}
