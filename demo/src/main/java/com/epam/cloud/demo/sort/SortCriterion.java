package com.epam.cloud.demo.sort;

import org.springframework.data.domain.Sort;

import static org.springframework.data.domain.Sort.Direction.*;


public class SortCriterion
{
	private String property;
	private Sort.Direction direction;

	public SortCriterion(String property, String direction)
	{
		this.property = property;
		this.direction = fromOptionalString(direction.toUpperCase()).orElse(DESC);
	}

	public String getProperty()
	{
		return property;
	}

	public Sort.Direction getDirection()
	{
		return direction;
	}
}
