package com.epam.cloud.demo.exceptions;

public class ProductNotFoundException extends RuntimeException
{

	public ProductNotFoundException()
	{
		super("Product with given id is not found");
	}

	public ProductNotFoundException(String message)
	{
		super(message);
	}

	public ProductNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public ProductNotFoundException(Throwable cause)
	{
		super(cause);
	}

	protected ProductNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
