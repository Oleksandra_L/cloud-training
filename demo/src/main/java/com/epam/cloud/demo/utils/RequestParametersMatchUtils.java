package com.epam.cloud.demo.utils;

import com.epam.cloud.demo.entities.Product;
import com.epam.cloud.demo.search.SpecificationBuilder;
import com.epam.cloud.demo.sort.SortBuilder;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.epam.cloud.demo.search.ProductSpecificationHolder.PRODUCT_PROPERTY_SPECIFICATION_MAP;
import static io.micrometer.core.instrument.util.StringUtils.isBlank;


public class RequestParametersMatchUtils
{

	private static final String SORT_PATTERN = "(\\w+?):(\\w+?),";
	private static final String SEARCH_OPERATION_PATTERN = "(\\w+?)(:|<|>)(\\w+?),";

	public static Sort buildSortCriteria(String sort)
	{
		SortBuilder sortBuilder = new SortBuilder();
		Pattern pattern = Pattern.compile(SORT_PATTERN);
		Matcher matcher = pattern.matcher(sort + ",");
		while (matcher.find())
		{
			sortBuilder.with(matcher.group(1), matcher.group(2));
		}
		return sortBuilder.build();
	}

	public static Optional<Specification<Product>> buildSearchSpecification(String search)
	{
		if (isBlank(search))
		{
			return Optional.empty();
		}
		SpecificationBuilder<Product> builder = new SpecificationBuilder<>(PRODUCT_PROPERTY_SPECIFICATION_MAP);
		Pattern pattern = Pattern.compile(SEARCH_OPERATION_PATTERN);
		Matcher matcher = pattern.matcher(search + ",");
		while (matcher.find())
		{
			builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
		}
		return builder.build();
	}

}
