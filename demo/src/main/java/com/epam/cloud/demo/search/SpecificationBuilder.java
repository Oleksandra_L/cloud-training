package com.epam.cloud.demo.search;

import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;


public class SpecificationBuilder<T>
{
	private Map<String, Function<String, Specification<T>>> specificationMap;
	private final List<SearchCriterion> searchCriteria;

	public SpecificationBuilder(Map<String, Function<String, Specification<T>>> searchTypeMapper)
	{
		this.searchCriteria = new ArrayList<SearchCriterion>();
		this.specificationMap = searchTypeMapper;
	}

	public SpecificationBuilder with(String key, String operation, String value)
	{
		searchCriteria.add(new SearchCriterion(key, operation, value));
		return this;
	}

	public Optional<Specification<T>> build()
	{
		if (searchCriteria.size() == 0)
		{
			return Optional.empty();
		}

		return mergeSpecifications();
	}

	private Optional<Specification<T>> mergeSpecifications()
	{
		List<Specification<T>> specs = searchCriteria.stream()
				.map(this::mapToSpecification)
				.collect(Collectors.toList());

		Specification<T> specification = specs.get(0);
		for (int i = 1; i < searchCriteria.size(); i++)
		{
			specification = Specification.where(specification)
					.and(specs.get(i));
		}
		return Optional.of(specification);
	}

	private Specification<T> mapToSpecification(SearchCriterion searchCriteria)
	{
		return specificationMap.get(searchCriteria.getKey() + searchCriteria.getOperation())
				.apply(searchCriteria.getValue());
	}
}
