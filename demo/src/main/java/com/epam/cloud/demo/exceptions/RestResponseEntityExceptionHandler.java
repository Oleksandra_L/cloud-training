package com.epam.cloud.demo.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.UnsupportedEncodingException;


@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler
{

	@ExceptionHandler(value = { CategoryNotFoundException.class, ProductNotFoundException.class })
	protected ResponseEntity<Object> handleEntityConflict(
			RuntimeException ex, WebRequest request)
	{
		String bodyOfResponse = "No entity was found with such id in database";
		return handleExceptionInternal(ex, bodyOfResponse,
									   new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}

	@ExceptionHandler(UnsupportedEncodingException.class)
	protected ResponseEntity<Object> handleDecodeConflict(
			RuntimeException ex, WebRequest request)
	{
		return handleExceptionInternal(ex, ex.getMessage(),
									   new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

	@ExceptionHandler(Exception.class)
	protected ResponseEntity<Object> handleDefaultConflict(
			RuntimeException ex, WebRequest request)
	{
		return handleExceptionInternal(ex, ex.getMessage(),
									   new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
}
