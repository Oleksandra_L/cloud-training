package com.epam.cloud.demo.services;

import com.epam.cloud.demo.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;


public interface ProductService
{
	Page<Product> getAll(Pageable pageable);

	List<Product> getAll(Specification<Product> specification);

	Page<Product> getAll(Pageable pageable, Specification<Product> specification);

	Product save(Product product);

	Product get(Long id);

	void delete(Long id);

	Product assignCategory(long productId, long categoryId);

	Product unassignCategory(long productId, long categoryId);

	Product update(long id, Product product);
}
