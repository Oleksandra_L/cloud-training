package com.epam.cloud.demo.controllers;

import com.epam.cloud.demo.entities.Product;
import com.epam.cloud.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static com.epam.cloud.demo.search.ProductSpecificationHolder.belongsToCategory;
import static com.epam.cloud.demo.utils.RequestParametersMatchUtils.buildSearchSpecification;
import static com.epam.cloud.demo.utils.RequestParametersMatchUtils.buildSortCriteria;


/**
 * Product controller
 */
@RestController
@RequestMapping(value = "/api")
public class ProductController
{

	@Autowired
	private ProductService productService;

	@GetMapping("/products")
	public Page<Product> getProducts(@RequestParam(value = "search", required = false) String search,
			@RequestParam(required = false) String sort, @RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "10") int size) throws UnsupportedEncodingException
	{
		String decodedSearch = search != null ? URLDecoder.decode(search, StandardCharsets.UTF_8.name()) : search;
		Pageable pageable = PageRequest.of(page, size, buildSortCriteria(sort));
		Optional<Specification<Product>> specification = buildSearchSpecification(decodedSearch);

		return specification.isPresent()
				? productService.getAll(pageable, specification.get())
				: productService.getAll(pageable);
	}

	@GetMapping("/products/product/{id}")
	public Product getProductById(@PathVariable long id)
	{
		return productService.get(id);
	}

	@GetMapping("/products/category/{categoryId}")
	public Page<Product> getProductsByCategoryId(@PathVariable long categoryId,
			@RequestParam(value = "search", required = false) String search,
			@RequestParam(required = false) String sort,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "10") int size) throws UnsupportedEncodingException
	{
		String decodedSearch = search != null ? URLDecoder.decode(search, StandardCharsets.UTF_8.name()) : search;
		String decodedSort = sort != null ? URLDecoder.decode(sort, StandardCharsets.UTF_8.name()) : sort;
		Pageable pageable = PageRequest.of(page, size, buildSortCriteria(decodedSort));
		Optional<Specification<Product>> specification = buildSearchSpecification(decodedSearch);

		return specification.isPresent()
				? productService.getAll(pageable, specification.get().and(belongsToCategory(categoryId)))
				: productService.getAll(pageable, belongsToCategory(categoryId));
	}

	@PostMapping("/product")
	public Product createProduct(@RequestBody Product product)
	{
		return productService.save(product);
	}

	@PutMapping("/product/{id}")
	public Product updateProduct(@PathVariable long id, @RequestBody Product product)
	{
		return productService.update(id, product);
	}

	@PutMapping("/product/{productId}/category/{categoryId}")
	public Product assignCategory(@PathVariable long productId, @PathVariable long categoryId)
	{
		return productService.assignCategory(productId, categoryId);
	}

	@DeleteMapping("/product/{productId}/category/{categoryId}")
	public Product unassignCategory(@PathVariable long productId, @PathVariable long categoryId)
	{
		return productService.unassignCategory(productId, categoryId);
	}

	@DeleteMapping("/product/{id}")
	public void deleteProductById(@PathVariable long id)
	{
		productService.delete(id);
	}
}
