package com.epam.cloud.demo.controllers;

import com.epam.cloud.demo.entities.Category;
import com.epam.cloud.demo.services.CategoryService;
import com.epam.cloud.demo.utils.RequestParametersMatchUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import static org.apache.commons.lang3.StringUtils.EMPTY;


@RestController()
@RequestMapping(value = "/api", produces = "application/json")
public class CategoryController
{

	@Autowired
	private CategoryService categoryService;

	@GetMapping("/categories")
	public Page<Category> getCategories(@RequestParam(required = false) String sort,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "10") int size) throws UnsupportedEncodingException
	{
		String decodedSort = sort != null ? URLDecoder.decode(sort, StandardCharsets.UTF_8.name()) : sort;
		Pageable pageable = PageRequest.of(page, size, RequestParametersMatchUtils.buildSortCriteria(decodedSort));

		return categoryService.getAll(pageable);
	}

	@GetMapping("/category/{id}")
	public Category getCategoryById(@PathVariable long id)
	{
		return categoryService.get(id);
	}

	@PostMapping("/category")
	public Category createCategory(@RequestBody Category category)
	{
		return categoryService.save(category);
	}

	@PutMapping("/category/{id}")
	public Category updateCategory(@PathVariable long id, @RequestBody Category category)
	{
		return categoryService.update(id, category);
	}

	@DeleteMapping("/category/{id}")
	public void deleteCategoryById(@PathVariable long id)
	{
		categoryService.delete(id);
	}
}
