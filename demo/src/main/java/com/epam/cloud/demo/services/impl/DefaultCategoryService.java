package com.epam.cloud.demo.services.impl;

import com.epam.cloud.demo.dao.CategoryRepository;
import com.epam.cloud.demo.entities.Category;
import com.epam.cloud.demo.exceptions.CategoryNotFoundException;
import com.epam.cloud.demo.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DefaultCategoryService implements CategoryService
{

	@Autowired
	private CategoryRepository categoryRepository;

	public List<Category> getAll()
	{
		return categoryRepository.findAll();
	}

	public List<Category> getAll(Sort sort)
	{
		return categoryRepository.findAll(sort);
	}

	public Page<Category> getAll(Pageable pageable)
	{
		return categoryRepository.findAll(pageable);
	}

	public Category save(Category category)
	{
		return categoryRepository.save(category);
	}

	public Category get(Long id)
	{
		return categoryRepository.findById(id).orElseThrow(CategoryNotFoundException::new);
	}

	public void delete(Long id)
	{
		Category category = get(id);
		category.getProducts().stream().forEach(product -> category.removeProduct(product));
		categoryRepository.deleteById(id);
	}

	@Override
	public Category update(long id, Category category)
	{
		Category categoryToUpdate = get(id);
		categoryToUpdate.setName(category.getName());
		categoryToUpdate.setDescription(category.getDescription());
		return categoryRepository.save(categoryToUpdate);
	}
}
