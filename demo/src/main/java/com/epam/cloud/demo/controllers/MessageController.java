package com.epam.cloud.demo.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Handle static message controller
 */
@RestController
@PropertySource("classpath:message.properties")
public class MessageController
{

	@Value("${message.text}")
	private String message;

	/**
	 * Read message from property file
	 *
	 * @return message from property file
	 */

	@GetMapping(value = { "/message" })
	public String message()
	{
		return message;
	}
}
