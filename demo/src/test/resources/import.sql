insert into product (id, description,name, price) values(1,  'Test description 1', 'The test name 1', 10.5);
insert into product (id, description,name, price) values(2,  'Test description 2', 'A test name 2', 20.5);


insert into category (id, description,name) values(1,  'Test category description 1', 'The test category name 1');
insert into category (id, description,name) values(2,  'Test category description 2', 'A test category name 2');

insert into product_category ( product_id,category_id) values(1, 1);
insert into product_category ( product_id,category_id) values(2, 1);
insert into product_category ( product_id,category_id) values(2, 2);

