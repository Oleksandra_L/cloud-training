package com.epam.cloud.demo.integration;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureMockMvc
public class CategoriesControllerIntegrationTest
{

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@BeforeEach
	public void setup()
	{
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void shouldReturnAllCategories() throws Exception
	{
		mockMvc.perform(get("/api/categories"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[?(@.id == 1)].name").value("The test category name 1"))
				.andExpect(jsonPath("$.content[?(@.id == 1)].description").value("Test category description 1"))
				.andExpect(jsonPath("$.content[?(@.id == 1)].totalProducts").value(2))
				.andExpect(jsonPath("$.content[?(@.id == 2)].name").value("A test category name 2"))
				.andExpect(jsonPath("$.content[?(@.id == 2)].description").value("Test category description 2"))
				.andExpect(jsonPath("$.content[?(@.id == 2)].totalProducts").value(1));
	}

	@Test
	public void shouldReturnAllCategoriesSortedByNameAcs() throws Exception
	{
		mockMvc.perform(get("/api/categories?sort=name:asc"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[0].name").value("A test category name 2"))
				.andExpect(jsonPath("$.content[0].description").value("Test category description 2"))
				.andExpect(jsonPath("$.content[0].totalProducts").value(1))
				.andExpect(jsonPath("$.content[1].name").value("The test category name 1"))
				.andExpect(jsonPath("$.content[1].description").value("Test category description 1"))
				.andExpect(jsonPath("$.content[1].totalProducts").value(2));
	}

	@Test
	public void shouldReturnAllCategoriesSortedByNameDesc() throws Exception
	{
		mockMvc.perform(get("/api/categories?sort=name:desc"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[0].name").value("The test category name 1"))
				.andExpect(jsonPath("$.content[0].description").value("Test category description 1"))
				.andExpect(jsonPath("$.content[0].totalProducts").value(2))
				.andExpect(jsonPath("$.content[1].name").value("A test category name 2"))
				.andExpect(jsonPath("$.content[1].description").value("Test category description 2"))
				.andExpect(jsonPath("$.content[1].totalProducts").value(1));
	}

	@Test
	public void shouldReturnAllCategoriesSortedByTotalProductsDesc() throws Exception
	{
		mockMvc.perform(get("/api/categories?sort=totalProducts:desc"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[0].totalProducts").value(2))
				.andExpect(jsonPath("$.content[1].totalProducts").value(1));
	}

	@Test
	public void shouldReturnAllCategoriesSortedByTotalProductsAsc() throws Exception
	{
		mockMvc.perform(get("/api/categories?sort=totalProducts:asc"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[0].totalProducts").value(1))
				.andExpect(jsonPath("$.content[1].totalProducts").value(2));
	}

	@Test
	public void shouldReturnAllCategoriesSortedByDecodedNameDesc() throws Exception
	{
		mockMvc.perform(get("/api/categories?sort=name%3Adesc"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[0].name").value("The test category name 1"))
				.andExpect(jsonPath("$.content[0].description").value("Test category description 1"))
				.andExpect(jsonPath("$.content[0].totalProducts").value(2))
				.andExpect(jsonPath("$.content[1].name").value("A test category name 2"))
				.andExpect(jsonPath("$.content[1].description").value("Test category description 2"))
				.andExpect(jsonPath("$.content[1].totalProducts").value(1));
	}

	@Test
	public void shouldReturnDefaultPageWithCategories() throws Exception
	{
		mockMvc.perform(get("/api/categories"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.totalElements").value(2))
				.andExpect(jsonPath("$.totalPages").value(1))
				.andExpect(jsonPath("$.size").value(10))
				.andExpect(jsonPath("$.number").value(0));
	}

	@Test
	public void shouldReturnCustomPageWithCategories() throws Exception
	{
		mockMvc.perform(get("/api/categories?page=1&size=1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.totalElements").value(2))
				.andExpect(jsonPath("$.totalPages").value(2))
				.andExpect(jsonPath("$.size").value(1))
				.andExpect(jsonPath("$.number").value(1))
				.andExpect(jsonPath("$.content[0].name").value("A test category name 2"))
				.andExpect(jsonPath("$.content[0].description").value("Test category description 2"))
				.andExpect(jsonPath("$.content[0].totalProducts").value(1));
	}

	@Test
	public void shouldReturnCategoryById() throws Exception
	{
		mockMvc.perform(get("/api/category/2"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.id").value(2))
				.andExpect(jsonPath("$.name").value("A test category name 2"))
				.andExpect(jsonPath("$.description").value("Test category description 2"))
				.andExpect(jsonPath("$.totalProducts").value(1));
	}

	@Test
	public void shouldReturn404WhenRequestAbsentCategory() throws Exception
	{
		mockMvc.perform(get("/api/category/10"))
				.andExpect(status().isNotFound());
	}

	@Test
	public void shouldDeleteCategoryById() throws Exception
	{
		mockMvc.perform(delete("/api/category/2"))
				.andExpect(status().isOk());

		mockMvc.perform(get("/api/category/2"))
				.andExpect(status().isNotFound());
	}

	@Test
	public void shouldReturnNotFoundWhenNoSuchCategories() throws Exception
	{
		mockMvc.perform(get("/api/category/100"))
				.andExpect(status().isNotFound());
	}

	@Test
	public void shouldUpdateCategoryById() throws Exception
	{
		mockMvc.perform(
				put("/api/category/1")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{\"name\":\"New category name\",\"description\":\"New category description\"}"))
				.andExpect(status().isOk());

		mockMvc.perform(get("/api/category/1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.name").value("New category name"))
				.andExpect(jsonPath("$.description").value("New category description"))
				.andExpect(jsonPath("$.totalProducts").value(2));
	}

	@Test
	public void shouldCreateCategory() throws Exception
	{
		mockMvc.perform(
				post("/api/category")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{\"name\":\"New category name\",\"description\":\"New category description\",\"id\":3}"))
				.andExpect(status().isOk());

		mockMvc.perform(get("/api/category/3"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.id").value(3))
				.andExpect(jsonPath("$.name").value("New category name"))
				.andExpect(jsonPath("$.description").value("New category description"))
				.andExpect(jsonPath("$.totalProducts").value(0));
	}

}
