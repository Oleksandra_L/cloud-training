package com.epam.cloud.demo.integration;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductControllerIntegrationTest
{

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@BeforeEach
	public void setup()
	{
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void shouldReturnAllProducts() throws Exception
	{
		mockMvc.perform(get("/api/products"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[?(@.id == 1)].name").value("The test name 1"))
				.andExpect(jsonPath("$.content[?(@.id == 1)].description").value("Test description 1"))
				.andExpect(jsonPath("$.content[?(@.id == 1)].price").value(10.5))
				.andExpect(jsonPath("$.content[?(@.id == 1)].categories.size()").value(1))
				.andExpect(jsonPath("$.content[?(@.id == 1)].categories[0].id").value(1))
				.andExpect(jsonPath("$.content[?(@.id == 2)].name").value("A test name 2"))
				.andExpect(jsonPath("$.content[?(@.id == 2)].description").value("Test description 2"))
				.andExpect(jsonPath("$.content[?(@.id == 2)].price").value(20.5))
				.andExpect(jsonPath("$.content[?(@.id == 2)].categories.size()").value(2))
				.andExpect(jsonPath("$.content[?(@.id == 2)].categories[?(@.id == 1)].name").exists())
				.andExpect(jsonPath("$.content[?(@.id == 2)].categories[?(@.id == 2)].name").exists());
	}

	@Test
	public void shouldReturnAllProductsSortedByNameAcs() throws Exception
	{
		mockMvc.perform(get("/api/products?sort=name:asc"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[0].name").value("A test name 2"))
				.andExpect(jsonPath("$.content[1].name").value("The test name 1"));
	}

	@Test
	public void shouldReturnAllProductsSortedByNameDecs() throws Exception
	{
		mockMvc.perform(get("/api/products?sort=name:decs"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[0].name").value("The test name 1"))
				.andExpect(jsonPath("$.content[1].name").value("A test name 2"));
	}

	@Test
	public void shouldFilterProductsByName() throws Exception
	{
		mockMvc.perform(get("/api/products?search=name:2"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content.size()").value(1))
				.andExpect(jsonPath("$.content[0].name").value("A test name 2"));
	}

	@Test
	public void shouldFilterProductsByPrice() throws Exception
	{
		mockMvc.perform(get("/api/products?search=price>10,price<20"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content.size()").value(1))
				.andExpect(jsonPath("$.content[0].name").value("The test name 1"));
	}

	@Test
	public void shouldReturnProductsByCategoryId() throws Exception
	{
		mockMvc.perform(get("/api/products/category/2"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[?(@.id == 2)].name").value("A test name 2"))
				.andExpect(jsonPath("$.content[?(@.id == 2)].description").value("Test description 2"))
				.andExpect(jsonPath("$.content[?(@.id == 2)].price").value(20.5))
				.andExpect(jsonPath("$.content[?(@.id == 2)].categories.size()").value(2))
				.andExpect(jsonPath("$.content.size()").value(1));
	}

	@Test
	public void shouldReturnProductsByCategoryIdSortedByNameAcs() throws Exception
	{
		mockMvc.perform(get("/api/products/category/1?sort=name:asc"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[0].name").value("A test name 2"))
				.andExpect(jsonPath("$.content[1].name").value("The test name 1"));
	}

	@Test
	public void shouldReturnProductsByCategoryIdSortedByNameDecs() throws Exception
	{
		mockMvc.perform(get("/api/products/category/1?sort=name:decs"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.content[0].name").value("The test name 1"))
				.andExpect(jsonPath("$.content[1].name").value("A test name 2"));
	}

		@Test
		public void shouldReturnProductsByCategoryIdFilteredByName () throws Exception
		{
			mockMvc.perform(get("/api/products/category/1?search=name:2"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.content.size()").value(1))
					.andExpect(jsonPath("$.content[0].name").value("A test name 2"));
		}

		@Test
		public void shouldReturnProductsByCategoryIdFilteredByDecodedName () throws Exception
		{
			mockMvc.perform(get("/api/products/category/1?search=name%3A2"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.content.size()").value(1))
					.andExpect(jsonPath("$.content[0].name").value("A test name 2"));
		}

		@Test
		public void shouldReturnProductsByCategoryIdFilteredByPrice () throws Exception
		{
			mockMvc.perform(get("/api/products/category/1?search=price%3E10%26price%3C20"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.content.size()").value(1))
					.andExpect(jsonPath("$.content[0].name").value("The test name 1"));
		}

		@Test
		public void shouldReturnProductsByCategoryIdFilteredByDecodedPrice () throws Exception
		{
			mockMvc.perform(get("/api/products/category/1?search=price>10,price<20"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.content.size()").value(1))
					.andExpect(jsonPath("$.content[0].name").value("The test name 1"));
		}

		@Test
		public void shouldReturnDefaultPageWithProducts () throws Exception
		{
			mockMvc.perform(get("/api/products"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.totalElements").value(2))
					.andExpect(jsonPath("$.totalPages").value(1))
					.andExpect(jsonPath("$.size").value(10))
					.andExpect(jsonPath("$.number").value(0));
		}

		@Test
		public void shouldReturnCustomPageWithProducts () throws Exception
		{
			mockMvc.perform(get("/api/products?page=1&size=1"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.totalElements").value(2))
					.andExpect(jsonPath("$.totalPages").value(2))
					.andExpect(jsonPath("$.size").value(1))
					.andExpect(jsonPath("$.number").value(1))
					.andExpect(jsonPath("$.content[0].name").value("A test name 2"));
		}

		@Test
		public void shouldReturnProductById () throws Exception
		{
			mockMvc.perform(get("/api/products/product/2"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.id").value(2))
					.andExpect(jsonPath("$.name").value("A test name 2"))
					.andExpect(jsonPath("$.description").value("Test description 2"))
					.andExpect(jsonPath("$.price").value(20.5))
					.andExpect(jsonPath("$.categories.size()").value(2))
					.andExpect(jsonPath("$.categories[?(@.id==1)].name").value("The test category name 1"))
					.andExpect(jsonPath("$.categories[?(@.id==2)].name").value("A test category name 2"));
		}

		@Test
		public void shouldReturn404WhenRequestAbsentProduct () throws Exception
		{
			mockMvc.perform(get("/api/products/product/10"))
					.andExpect(status().isNotFound());
		}

		@Test
		public void shouldDeleteProductById () throws Exception
		{
			mockMvc.perform(delete("/api/product/2"))
					.andExpect(status().isOk());

			mockMvc.perform(get("/api/products/product/2"))
					.andExpect(status().isNotFound());
		}

		@Test
		public void shouldReturnNotFoundWhenNoSuchProducts () throws Exception
		{
			mockMvc.perform(get("/api/products/product/100"))
					.andExpect(status().isNotFound());
		}

		@Test
		public void shouldUpdateProductById () throws Exception
		{
			mockMvc.perform(
					put("/api/product/1")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\"name\":\"New name\",\"description\":\"New description\"}"))
					.andExpect(status().isOk());

			mockMvc.perform(get("/api/products/product/1"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.id").value(1))
					.andExpect(jsonPath("$.name").value("New name"))
					.andExpect(jsonPath("$.description").value("New description"))
					.andExpect(jsonPath("$.price").value(10.5))
					.andExpect(jsonPath("$.categories.size()").value(1))
					.andExpect(jsonPath("$.categories[0].id").value(1));
		}

		@Test
		public void shouldCreateProduct () throws Exception
		{
			mockMvc.perform(
					post("/api/product")
							.contentType(MediaType.APPLICATION_JSON)
							.content("{\"name\":\"New name\",\"description\":\"New description\",\"id\":3}"))
					.andExpect(status().isOk());

			mockMvc.perform(get("/api/products/product/3"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.id").value(3))
					.andExpect(jsonPath("$.name").value("New name"))
					.andExpect(jsonPath("$.description").value("New description"))
					.andExpect(jsonPath("$.price").doesNotExist())
					.andExpect(jsonPath("$.categories.size()").value(0));
		}


		@Test
		public void shouldUnassignCategory () throws Exception
		{
			mockMvc.perform(
					delete("/api/product/1/category/1"))
					.andExpect(status().isOk());

			mockMvc.perform(get("/api/products/product/1"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.id").value(1))
					.andExpect(jsonPath("$.categories.size()").value(0));
		}


		@Test
		public void shouldAssignCategory () throws Exception
		{
			mockMvc.perform(
					put("/api/product/1/category/2"))
					.andExpect(status().isOk());

			mockMvc.perform(get("/api/products/product/1"))
					.andExpect(status().isOk())
					.andExpect(content().contentType("application/json"))
					.andExpect(jsonPath("$.id").value(1))
					.andExpect(jsonPath("$.categories.size()").value(2));
		}
	}
