# Module 1

# 3. Deployment to cloud provider
- Swagger UI page http://ec2-15-236-186-4.eu-west-3.compute.amazonaws.com/swagger-ui.html
- OpenAPI description http://ec2-15-236-186-4.eu-west-3.compute.amazonaws.com/v3/api-docs
- OpenAPI description in yaml formathttp://ec2-15-236-186-4.eu-west-3.compute.amazonaws.com/v3/api-docs.yaml
- Entry point http://ec2-15-236-186-4.eu-west-3.compute.amazonaws.com/
- Message pointhttp://ec2-15-236-186-4.eu-west-3.compute.amazonaws.com/message


# Script for deploying image on EC2 instance

- $ sudo yum update -y
- $ sudo amazon-linux-extras install docker
- $ sudo service docker start
- $ sudo docker run -d -p 80:8080 oleksandralukianova/public-repo


# Module 2


# Install SSM Agent on Amazon Linux 2 EC2 instance
- $ sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
- $ sudo systemctl status amazon-ssm-agent
- $ sudo systemctl enable amazon-ssm-agent
- $ sudo systemctl start amazon-ssm-agent

# Create an IAM instance profile for Systems Manager
Used manual https://docs.amazonaws.cn/en_us/systems-manager/latest/userguide/setup-instance-profile.html


#Product filter endpoints examples
- http://ec2-15-236-186-4.eu-west-3.compute.amazonaws.com/api/products?search=name:Effective,price>10,price<20
- http://ec2-15-236-186-4.eu-west-3.compute.amazonaws.com/api/categories?sort=totalProducts:desc
